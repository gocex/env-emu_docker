# Environment emulator

This repository contains the base server environment for the environment emulator of gocex-rover project.

As the emulator is PHP based and have to use mongoDB it isn't possible to use the  base composer image. Please, build
and use the images/composer/Dockerfile to create your composer image.

The env-emu project is expected to be located in the same folder than the env-emu_docker project. If not, the 
docker-compose.yml descriptors have to be updated.

To start the docker environment, execute `docker-compose up` in the env-emu_dev folder.
